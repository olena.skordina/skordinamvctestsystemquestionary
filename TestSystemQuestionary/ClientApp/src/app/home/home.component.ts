import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TestService } from '../services/test.service';
import { TestRun, Tests, Token } from '../types';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  tests: Tests;
  testRun: TestRun;

  constructor( private TestService: TestService, private router : Router) {
  }

  ngOnInit(): void {
    this.TestService.getAllTests().subscribe(x=>this.tests = x)
  }

  startTestRun(testId : number) {
    this.router.navigate(["testrun", {
      id: testId
    } ])
  }
}