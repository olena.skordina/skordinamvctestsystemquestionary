import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { User } from 'oidc-client';
import { Observable } from 'rxjs';
import { TestResult, TestRun, Tests, Token, UserUI } from '../types';

@Injectable({
  providedIn: 'root'
})

export class TestService {

  baseUrl: string;
  public tests: Tests;
  public testRun: TestRun;

  
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getAllTests(): Observable<Tests> {

     return this.http.get<Tests>(this.baseUrl + 'api/tests', {headers: this.GetHeaders()});
  }

  startTest(testId : number) : Observable<TestRun> {
    return this.http.post<TestRun>(this.baseUrl + 'api/testrun/start/' + testId, null, {headers: this.GetHeaders()});
  }

  Login(login : string, password : string): Observable<Token> {
    return this.http.post<Token>(this.baseUrl + 'api/auth/token', {
      username : login,
      password : password
    }, {headers: this.GetHeaders()});
  }

  private GetHeaders(): HttpHeaders {
    return new HttpHeaders().set('Authorization', "Bearer " + localStorage.getItem("token"));
  }

  saveTestRun(testRun : TestRun){
    return this.http.post<TestResult>(this.baseUrl + 'api/testrun', testRun, {headers: this.GetHeaders()});
  }
}
