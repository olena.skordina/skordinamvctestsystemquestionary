import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TestService } from '../services/test.service';
import { TestResult, TestRun, Tests } from '../types';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-test-run',
  templateUrl: './test-run.component.html'
})
export class TestRunComponent implements OnInit {

  testRun: TestRun;
  testResult: TestResult;

  constructor( private TestService: TestService, private activatedRoute : ActivatedRoute) {
  }

  ngOnInit(): void {
    const testId = this.activatedRoute.snapshot.paramMap.get('id');
    this.startTestRun(+testId);
  }

  private startTestRun(testId : number) {
    this.TestService.startTest(testId).subscribe(x=>this.testRun = x)
  }

  saveResult() {
    this.TestService.saveTestRun(this.testRun).subscribe(x=>this.testResult = x)
  }
}
