
export interface Tests {
    tests : Test[];
 }
 
 export interface Test {
   id: number;
   testName: string;
 }
 
 export interface Answer {
   id: number;
   answerText: string;
   isChecked: boolean;
 }
 
 export interface Question {
   id: number;
   questionText: string;
   answers: Answer[];
 }
 
 export interface TestRun {
   testRunId: number;
   testName: string;
   questions: Question[];
 }

 export interface UserUI {
     login : string;
     password : string;
 }

 export interface Token {
    access_token : string;
    username : string;
}

export interface TestResult {
    testName : string;
    score : number;
    passed : boolean;
}