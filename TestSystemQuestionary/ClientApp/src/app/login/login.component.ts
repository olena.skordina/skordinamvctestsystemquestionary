import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Token } from '../types';
import { TestService } from '../services/test.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  
  login: string;
  password: string;
  token : Token;
  
  constructor( private TestService: TestService) {
  }

  Login(login : string, password : string) {
  this.TestService.Login(login,password).subscribe(x=> {
      this.token = x;
      localStorage.setItem("token",this.token.access_token);
    });
   }
}
