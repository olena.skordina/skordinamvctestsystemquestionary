﻿using Business.DTO;
using Business.DTO.SetupDTOs;
using Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TestSystemQuestionary.Controllers
{
    [Authorize(Roles="admin")]
    [Route("api/setup")]
    [ApiController]
    public class TestSetupController : ControllerBase
    {
        private readonly ITestSetupService testSetupService;
        public TestSetupController(ITestSetupService testSetupService)
        {
            this.testSetupService = testSetupService;
        }

        // GET: TestSetupController --> all tests
        [Route("tests")]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var tests = await testSetupService.ListOfTestDTO();
            return Ok(new ListOfTestsDTO {Tests = tests});
        }

        // GET: TestSetupController --> one test
        [Route("test/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetTestById(int id)
        {
            var test = await testSetupService.GetTestById(id);
            return Ok(test);
        }

        [Route("test")]
        [HttpPost]
        public async Task<IActionResult> SaveTest(TestSetupDTO test)
        {
            await testSetupService.SaveTestToDB(test);
            return Ok();
        }

        //PUT
        [Route("test")]  //need to find how to track deleted questions/answers, as of now it doesn't delete them on update
        [HttpPut]
        public async Task<IActionResult> UpdateTest(TestSetupDTO test)
        {
            await testSetupService.UpdateTest(test);
            return Ok();
        }

        //DELETE
        [Route("test/{id}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteTestSoft(int id)
        {
            await testSetupService.SoftDeleteById(id);
            return Ok();
        }
    }
}
