﻿using Business.DTO;
using Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using TestSystemQuestionary.Configuration;

namespace TestSystemQuestionary.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [Route("token")]
        [HttpPost]
        public async Task<IActionResult> Token([FromBody] UserDTO user)
        {
            var identity = await GetIdentity(user.Username, user.Password);
            if (identity == null)
            {
                return BadRequest(new { errorText = "Invalid username or password" });
            }
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(
                    AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };
            return Json(response);
        }

        private async Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            var person =await _userService.GetUserByCredentials(username, password);
            if (person != null)
            {
                var claims = new List<Claim>
            {
                new Claim( ClaimsIdentity.DefaultNameClaimType, person.Username),
                new Claim( ClaimsIdentity.DefaultRoleClaimType, person.Role.RoleName)
            };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, " Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            // if user not found
            return null;
        }
    }
}
