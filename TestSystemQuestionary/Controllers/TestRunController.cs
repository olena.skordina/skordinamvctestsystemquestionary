﻿using Business.DTO;
using Business.DTO.TestRunDTOs;
using Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TestSystemQuestionary.Controllers
{
    [Route("api")]
    [ApiController]
    [Authorize(Roles = "user, admin")]
    public class TestRunController : ControllerBase
    {
        private readonly ITestRunService testRunService;
        private readonly ITestSetupService testSetupService;
        public TestRunController(ITestRunService testRunService, ITestSetupService testSetupService)
        {
            this.testRunService = testRunService;
            this.testSetupService = testSetupService;
        }

        // GET: TestRunController --> all tests
        [Route("tests")]
        [HttpGet]
        public async Task<IActionResult> AllTests()
        {
            var tests = await testSetupService.ListOfTestDTO();
            return Ok(new ListOfTestsDTO { Tests = tests });
        }

        [Route("testruns")]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var testRuns = await testRunService.ListOfTestRunsDTO();
            return Ok(new ListOfTestRunsDTO {TestRuns = testRuns});
        }

        [Route("testrun/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetTestRunById(int id)
        {
            var test = await testRunService.GetTestRunById(id);
            return Ok(test);
        }

        [Route("testrun/start/{id}")]
        [HttpPost]
        public async Task<IActionResult> StartTestRunById(int id)
        {
            var startTestRun = await testRunService.StartTestRunById(id);
            return Ok(startTestRun);
        }

        [Route("testrun")]
        [HttpPost]
        public async Task<IActionResult> SaveTestRun([FromBody]TestRunDTO test)
        {
            var returnedTestRunResult = await testRunService.SaveTestRunToDB(test);
            return Ok(returnedTestRunResult);
        }
    }
}
