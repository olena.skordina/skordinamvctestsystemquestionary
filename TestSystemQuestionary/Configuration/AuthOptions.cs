﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystemQuestionary.Configuration
{
    public class AuthOptions
    {
        // token publisher
        public const string ISSUER = "WebApplicationTestSystemServer";
        // token user
        public const string AUDIENCE = "WebApplicationTestSystemClient";
        // key for encrypting
        const string KEY = "TestSystem_secretkey! 123";
        // token ttl 30 minute
        public const int LIFETIME = 30;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
