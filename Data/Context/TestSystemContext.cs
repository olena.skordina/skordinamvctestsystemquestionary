﻿using Data.Config;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;

namespace Data.Context
{
    public class TestSystemContext : DbContext
    {
        public DbSet<RoleEntity> Roles { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<AnswerOptionEntity> Answers { get; set; }
        public DbSet<QuestionEntity> Questions { get; set; }
        public DbSet<TestEntity> Tests { get; set; }
        public DbSet<TestResultEntity> TestResults { get; set; }
        public DbSet<TestRunEntity> TestRuns { get; set; }

        private readonly DatabaseConfig _config;

        public TestSystemContext(IOptions<DatabaseConfig> config)
        {
            _config = config.Value ?? throw new ArgumentNullException(nameof(config));
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_config.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) // check if I can add SQL annotation on entity
        {
           modelBuilder.Entity<TestEntity>().Property(x => x.CreatedOn).HasDefaultValueSql("GETDATE()");
           modelBuilder.Entity<TestResultEntity>().Property(x => x.CreatedOn).HasDefaultValueSql("GETDATE()");
           modelBuilder.Entity<TestRunEntity>().Property(x => x.CreatedOn).HasDefaultValueSql("GETDATE()");
           modelBuilder.Entity<QuestionEntity>().Property(x => x.CreatedOn).HasDefaultValueSql("GETDATE()");
           modelBuilder.Entity<AnswerOptionEntity>().Property(x => x.CreatedOn).HasDefaultValueSql("GETDATE()");
           modelBuilder.Entity<UserEntity>().Property(x => x.CreatedOn).HasDefaultValueSql("GETDATE()");
           modelBuilder.Entity<RoleEntity>().Property(x => x.CreatedOn).HasDefaultValueSql("GETDATE()");
           modelBuilder.Entity<RoleEntity>().HasData(new RoleEntity { Id = 1,RoleName = "admin"});
           modelBuilder.Entity<RoleEntity>().HasData(new RoleEntity { Id = 2,RoleName = "user"});
           modelBuilder.Entity<UserEntity>().HasData(new UserEntity {Id = 1, Username = "admin@gmail.com", Password = "12345" ,RoleId = 1});
           modelBuilder.Entity<UserEntity>().HasData(new UserEntity { Id = 2, Username = "user@gmail.com", Password = "54321" ,RoleId = 2});
        }

    }
}
