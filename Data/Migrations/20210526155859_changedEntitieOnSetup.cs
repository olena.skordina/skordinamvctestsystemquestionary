﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class changedEntitieOnSetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Questions_QuestionEntityId",
                table: "Answers");

            migrationBuilder.DropForeignKey(
                name: "FK_Questions_Tests_TestEntityId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_Questions_TestEntityId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_Answers_QuestionEntityId",
                table: "Answers");

            migrationBuilder.DropColumn(
                name: "TestEntityId",
                table: "Questions");

            migrationBuilder.DropColumn(
                name: "QuestionEntityId",
                table: "Answers");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_TestId",
                table: "Questions",
                column: "TestId");

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionId",
                table: "Answers",
                column: "QuestionId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Answers_Questions_QuestionId",
            //    table: "Answers",
            //    column: "QuestionId",
            //    principalTable: "Questions",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Questions_Tests_TestId",
            //    table: "Questions",
            //    column: "TestId",
            //    principalTable: "Tests",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Answers_Questions_QuestionId",
            //    table: "Answers");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Questions_Tests_TestId",
            //    table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_Questions_TestId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_Answers_QuestionId",
                table: "Answers");

            migrationBuilder.AddColumn<int>(
                name: "TestEntityId",
                table: "Questions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "QuestionEntityId",
                table: "Answers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Questions_TestEntityId",
                table: "Questions",
                column: "TestEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionEntityId",
                table: "Answers",
                column: "QuestionEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Questions_QuestionEntityId",
                table: "Answers",
                column: "QuestionEntityId",
                principalTable: "Questions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Questions_Tests_TestEntityId",
                table: "Questions",
                column: "TestEntityId",
                principalTable: "Tests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
