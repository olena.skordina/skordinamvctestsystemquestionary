﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class removedmapperfortestrundtoandtestrun : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TestRuns_Tests_TestId",
                table: "TestRuns");

            migrationBuilder.DropIndex(
                name: "IX_TestRuns_TestId",
                table: "TestRuns");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_TestRuns_TestId",
                table: "TestRuns",
                column: "TestId");

            migrationBuilder.AddForeignKey(
                name: "FK_TestRuns_Tests_TestId",
                table: "TestRuns",
                column: "TestId",
                principalTable: "Tests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
