﻿using Data.Context;
using Data.Interfaces;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TestSystemContext _testSystemContext;

        public UnitOfWork(TestSystemContext context)
        {
            this._testSystemContext = context;
        }

        private ITestRepository _testRepository;
        public ITestRepository TestRepository
        {
            get
            {
                if (_testRepository == null)
                {
                    _testRepository = new TestRepository(_testSystemContext);
                }

                return _testRepository;
            }
        }


        private ITestRunRepository _testRunRepository;
        public ITestRunRepository TestRunRepository
        {
            get
            {
                if (_testRunRepository == null)
                {
                    _testRunRepository = new TestRunRepository(_testSystemContext);
                }

                return _testRunRepository;
            }
        }


        private ITestResultRepository _testResultRepository;
        public ITestResultRepository TestResultRepository
        {
            get
            {
                if (_testResultRepository == null)
                {
                    _testResultRepository = new TestResultRepository(_testSystemContext);
                }

                return _testResultRepository;
            }
        }
        private IUserRepository _userRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_testSystemContext);
                }

                return _userRepository;
            }
        }
        public Task<int> SaveAsync()
        {
            return _testSystemContext.SaveChangesAsync();
        }
    }
}
