﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class TestEntity : BaseEntity
    {
        [Column(TypeName = "VARCHAR(256)")]
        public string TestName { get; set; }
        public bool IsDraft { get; set; }
        public int Threshold { get; set; }
        public virtual ICollection<QuestionEntity> Questions { get; set; }
        public virtual ICollection<TestRunEntity> TestRuns { get; set; }
    }
}
