﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class AnswerOptionEntity : BaseEntity
    {
        [Column(TypeName = "VARCHAR(2048)")]
        public string AnswerText { get; set; }
        public bool IsCorrect { get; set; }
        public int QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public QuestionEntity QuestionEntity { get; set; }
       
    }
}
