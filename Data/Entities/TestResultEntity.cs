﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class TestResultEntity : BaseEntity //to see results of passing the test
    {
        public int TestId { get; set; }
        //user -- from created By ??
        public int QuestionId { get; set; }
        public bool IsCorrect { get; set; }

    }
}
