﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entities
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOn { get; set; } //to check if we update test, createdOn moved to default value for answers and questions
        public DateTime LastModifiedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public int LastModifiedBy { get; set; }
    }
}
