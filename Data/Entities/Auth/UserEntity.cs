﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class UserEntity : BaseEntity
    {
        [Column(TypeName = "VARCHAR(256)")]
        public string Username { get; set; }
        public string Password { get; set; } //my for test, not in migration
        public int RoleId { get; set; }
        public RoleEntity Role { get; set; }//my for test, not in migration

    }
}
