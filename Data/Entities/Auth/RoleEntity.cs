﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class RoleEntity : BaseEntity
    {
        [Column(TypeName = "VARCHAR(256)")]
        public string RoleName { get; set; }
        public UserEntity User { get; set; }
    }
}
