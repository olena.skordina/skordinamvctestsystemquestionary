﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class QuestionEntity : BaseEntity
    {
        [Column(TypeName = "VARCHAR(2048)")]
        public string QuestionText { get; set; }
        public int QuestionType { get; set; }
        public int TestId { get; set; }
        
        [ForeignKey("TestId")]
        public TestEntity TestEntity { get; set; }
        public virtual ICollection<AnswerOptionEntity> Answers { get; set; }
    }
}
