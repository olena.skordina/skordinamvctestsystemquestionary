﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class TestRunEntity : BaseEntity
    {
        public int TestId { get; set; }
        public int Score { get; set; }
        public DateTime TimeOfStart { get; set; }
        public DateTime? TimeOfCompletion { get; set; }

        [ForeignKey("TestId")]
        public TestEntity TestEntity { get; set; }
    }
}
