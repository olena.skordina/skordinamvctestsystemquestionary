﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class TestResultRepository : BaseRepository<TestResultEntity>, ITestResultRepository
    {
        private TestSystemContext _testSystemContext;

        public TestResultRepository(TestSystemContext testSystemContext) : base(testSystemContext)
        {
            _testSystemContext = testSystemContext;
        }

    }
}
