﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private TestSystemContext _testSystemContext;
        protected BaseRepository(TestSystemContext testSystemContext)
        {
            _testSystemContext = testSystemContext;
        }

        public async Task AddAsync(TEntity entity)
        {
            entity.LastModifiedOn = DateTime.Now;
            await _testSystemContext.AddAsync(entity);
        }

        public void Delete(TEntity entity)
        {
            _testSystemContext.Remove(entity);
        }

        public void SoftDeleteById(int id) //without cascade deletion
        {
            var entityToUpdateDeletedFlag = _testSystemContext.Set<TEntity>().FirstOrDefault(x => x.Id == id);
            entityToUpdateDeletedFlag.Deleted = true;
            Update(entityToUpdateDeletedFlag);
        }
        public async Task DeleteByIdAsync(int id)
        {
            var entity = _testSystemContext.Set<TEntity>().FirstOrDefault(x => x.Id == id);
            Delete(entity);
            await _testSystemContext.SaveChangesAsync();
        }

        public IQueryable<TEntity> FindAll()
        {
            return _testSystemContext.Set<TEntity>();
        }

        public Task<TEntity> GetByIdAsync(int id)
        {
            return _testSystemContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(TEntity entity)
        {
            entity.LastModifiedOn = DateTime.Now;
            _testSystemContext.Set<TEntity>().Update(entity);
        }

        public void Add(TEntity entity)
        {
            entity.LastModifiedOn = DateTime.Now;
            _testSystemContext.Set<TEntity>().Add(entity);
        }
    }
}
