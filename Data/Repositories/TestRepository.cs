﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class TestRepository : BaseRepository<TestEntity>,ITestRepository
    {
        private TestSystemContext _testSystemContext;

        public TestRepository(TestSystemContext testSystemContext):base(testSystemContext)
        {
            _testSystemContext = testSystemContext;
        }

        public IQueryable<TestEntity> GetListOfNotDeletedTests()
        {
            return FindAll().Where(x => !x.Deleted).AsNoTracking();
        }

        public Task<TestEntity> GetTestById(int testId)
        {
            return FindAll()
                .Include(x=>x.Questions)
                .ThenInclude(x=>x.Answers)
                .FirstOrDefaultAsync(x=>x.Id==testId && !x.Deleted);
        }
 
    }
}
