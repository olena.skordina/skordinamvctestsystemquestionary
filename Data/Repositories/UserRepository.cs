﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UserRepository: BaseRepository<UserEntity>, IUserRepository
    {
        private TestSystemContext _testSystemContext;

        public UserRepository(TestSystemContext testSystemContext):base(testSystemContext)
        {
            _testSystemContext = testSystemContext;
        }

        public Task<UserEntity> GetUserByCredentialsAsync(string login, string password)
        {
            return FindAll()
                .Include(x => x.Role)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Username == login && x.Password == password);
        }

        public Task<UserEntity> GetUserByLoginAsync(string login)
        {
            return FindAll()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Username == login);
        }
    }
}
