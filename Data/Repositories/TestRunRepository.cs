﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class TestRunRepository : BaseRepository<TestRunEntity>, ITestRunRepository
    {
        private TestSystemContext _testSystemContext;
        public TestRunRepository(TestSystemContext testSystemContext) : base(testSystemContext)
        {
            _testSystemContext = testSystemContext;
        }
        public IQueryable<TestRunEntity> GetListOfNotDeletedTestRuns()
        {
            return FindAll().Where(x => !x.Deleted).AsNoTracking();
        }
        public Task<TestRunEntity> GetTestRunById(int testRunId)
        {
            return FindAll()
                .Include(x => x.TestEntity)
                .ThenInclude(x=> x.Questions)
                .ThenInclude(x => x.Answers)
                .FirstOrDefaultAsync(x => x.Id == testRunId && !x.Deleted);
        }
    }
}
