﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Interfaces
{
    public interface ITestResultRepository :IRepository <TestResultEntity>
    {
    }
}
