﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUnitOfWork
    {
        ITestRepository TestRepository { get; }
        IUserRepository UserRepository { get; }
        ITestRunRepository TestRunRepository { get; }
        ITestResultRepository TestResultRepository { get; }
        Task<int> SaveAsync();
    }
}
