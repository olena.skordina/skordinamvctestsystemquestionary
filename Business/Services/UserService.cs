﻿using AutoMapper;
using Business.DTO;
using Business.Interfaces;
using Data.Entities;
using Data.Interfaces;
using System.Threading.Tasks;

namespace Business.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork UnitOfWork { get; set; }
        IMapper Mapper { get; set; }
        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            UnitOfWork = uow;
            Mapper = mapper;
        }

        public async Task SaveUserToDB(UserDTO user)
        {
            await UnitOfWork.UserRepository.AddAsync(Mapper.Map<UserEntity>(user));
            await UnitOfWork.SaveAsync();
        }

        public async Task<UserEntity> GetUserByCredentials(string login, string password)
        {
            return await UnitOfWork.UserRepository.GetUserByCredentialsAsync(login, password);
        }

    }
}
