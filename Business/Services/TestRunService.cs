﻿using AutoMapper;
using Business.DTO;
using Business.DTO.TestRunDTOs;
using Business.Interfaces;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Services
{
    public class TestRunService : ITestRunService
    {
        IUnitOfWork UnitOfWork { get; set; }
        IMapper Mapper { get; set; }
        public TestRunService(IUnitOfWork uow, IMapper mapper)
        {
            UnitOfWork = uow;
            Mapper = mapper;
        }

        public async Task<List<TestRunDTO>> ListOfTestRunsDTO()
        {
            return Mapper.Map<List<TestRunDTO>>(await UnitOfWork.TestRunRepository.GetListOfNotDeletedTestRuns()
                .ToListAsync());
        }

        public async Task<TestResultDTO> SaveTestRunToDB(TestRunDTO testRun)
        {
            var dbTestRun = await UnitOfWork.TestRunRepository.GetTestRunById(testRun.TestRunId);
            var passed = 0;

            foreach (var dbq in dbTestRun.TestEntity.Questions)
            {
                var answerQuestion = testRun.Questions.FirstOrDefault(trq => trq.Id == dbq.Id);
                if (answerQuestion is null)
                    continue;

                var selectedAnswers = answerQuestion.ListOfAnswers.Where(x => x.IsChecked).ToList();
                if (selectedAnswers.Count != 1)
                    continue;

                var selectedAnswerId = selectedAnswers.First().Id;
                var correctAnswerId = dbq.Answers.SingleOrDefault(x => x.IsCorrect).Id;

                if (selectedAnswerId == correctAnswerId)
                    passed++;
            }

            var score = passed * 100 / dbTestRun.TestEntity.Questions.Count;

            dbTestRun.Score = score;
            dbTestRun.TimeOfCompletion = DateTime.Now;
            UnitOfWork.TestRunRepository.Update(dbTestRun);
            await UnitOfWork.SaveAsync();

            return new TestResultDTO { 
                TestName = dbTestRun.TestEntity.TestName, 
                Score = dbTestRun.Score, 
                Passed = score > dbTestRun.TestEntity.Threshold };
        }
        
        public async Task<TestRunDTO> StartTestRunById(int testId)
        {
            var test = await UnitOfWork.TestRepository.GetTestById(testId);
            if (test is null)
                return null;

            var testRun = new TestRunEntity
            {
                TimeOfStart = DateTime.Now,
                TestId = test.Id
            };

            await UnitOfWork.TestRunRepository.AddAsync(testRun);
            await UnitOfWork.SaveAsync();

            var result = await UnitOfWork.TestRunRepository.GetTestRunById(testRun.Id);
            return Mapper.Map<TestRunDTO>(result);
        }

        public async Task<TestRunDTO> GetTestRunById(int testRunId)
        {
            return Mapper.Map<TestRunDTO>(await UnitOfWork.TestRunRepository.GetTestRunById(testRunId));
        }

        public async Task UpdateTestRun(TestRunDTO testRun)
        {
            UnitOfWork.TestRunRepository.Update(Mapper.Map<TestRunEntity>(testRun));
            await UnitOfWork.SaveAsync();
        }

        public async Task SoftDeleteById(int testRunId)
        {
            UnitOfWork.TestRunRepository.SoftDeleteById(testRunId);
            await UnitOfWork.SaveAsync();
        }
        
    }
}
