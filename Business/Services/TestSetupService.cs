﻿using AutoMapper;
using Business.DTO;
using Business.DTO.SetupDTOs;
using Business.Interfaces;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Services
{
    public class TestSetupService : ITestSetupService
    {
        IUnitOfWork UnitOfWork { get; set; }
        IMapper Mapper { get; set; }
        public TestSetupService(IUnitOfWork uow, IMapper mapper)
        {
            UnitOfWork = uow;
            Mapper = mapper;
        }

        public async Task<List<TestDTO>> ListOfTestDTO()
        {
            return Mapper.Map<List<TestDTO>>(await UnitOfWork.TestRepository.GetListOfNotDeletedTests()
                .ToListAsync());
        }

        public async Task SaveTestToDB(TestSetupDTO test) //question wasn't saved
        {
            await UnitOfWork.TestRepository.AddAsync(Mapper.Map<TestEntity>(test));
            await UnitOfWork.SaveAsync();
        }

        public async Task<TestSetupDTO> GetTestById(int testId)
        {
            return Mapper.Map<TestSetupDTO>(await UnitOfWork.TestRepository.GetTestById(testId));
        }

        public async Task UpdateTest(TestSetupDTO test)
        {
            UnitOfWork.TestRepository.Update(Mapper.Map<TestEntity>(test));
            await UnitOfWork.SaveAsync();
        }

        public async Task SoftDeleteById(int testId)
        {
            UnitOfWork.TestRepository.SoftDeleteById(testId);
            await UnitOfWork.SaveAsync();
        }
        
    }
}
