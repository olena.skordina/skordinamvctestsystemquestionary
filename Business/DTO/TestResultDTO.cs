﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DTO
{
    public class TestResultDTO
    {
        public string TestName { get; set; }
        public int Score { get; set; }
        public bool Passed { get; set; }
    }
}
