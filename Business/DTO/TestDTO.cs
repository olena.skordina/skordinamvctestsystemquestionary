﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DTO
{
    public class TestDTO
    {
        public int Id { get; set; }
        public string TestName { get; set; }
    }
}
