﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DTO
{
    public class ListOfTestsDTO
    {
        public List<TestDTO> Tests { get; set; }
    }
}
