﻿using Business.DTO.TestRunDTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DTO
{
    public class ListOfTestRunsDTO
    {
        public List<TestRunDTO> TestRuns { get; set; }
    }
}
