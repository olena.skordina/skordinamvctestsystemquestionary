﻿namespace Business.DTO.TestRunDTOs
{
    public class AnswerDTO
    {
        public int Id { get; set; }
        public string AnswerText { get; set; }
        public bool IsChecked { get; set; }
    }

}
