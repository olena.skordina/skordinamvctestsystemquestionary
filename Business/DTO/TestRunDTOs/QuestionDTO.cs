﻿using Data.Entities;
using System.Collections.Generic;

namespace Business.DTO.TestRunDTOs
{
    public class QuestionDTO
    {
        public int Id { get; set; }
        public List<AnswerDTO> ListOfAnswers { get; set; }
        public string QuestionText { get; set; }

    }

}
