﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DTO.TestRunDTOs
{
    public class TestRunDTO
    {
        public int TestRunId { get; set; }
        public string TestName { get; set; }
        public List<QuestionDTO> Questions { get; set; }
    }

}
