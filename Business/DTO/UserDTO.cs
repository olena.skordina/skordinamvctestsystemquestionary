﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.DTO
{
    public class UserDTO
    {
        [Required] // if not sent --> 400, can add a message
        [MinLength(1)]
        public string Username { get; set; }
        [Required] // if not sent --> 400, can add a message
        [MinLength(1)]
        public string Password { get; set; }
    }
}
