﻿using System.Collections.Generic;

namespace Business.DTO.SetupDTOs
{
    public class QuestionSetupDTO
    {
        public int? Id { get; set; }
        public string QuestionText { get; set; }
        public List<AnswerSetupDTO> Answers { get; set; }
        
    }

}
