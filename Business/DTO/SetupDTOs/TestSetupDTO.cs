﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.DTO.SetupDTOs
{
    public class TestSetupDTO
    {
        public int? Id { get; set; }

        [Required] // if not sent --> 400, can add a message
        [MinLength(1)]
        [MaxLength(256)]
        public string TestName { get; set; }

        [Required] // if not sent --> 400, can add a message
        [Range(1,100)]
        public int Threshold { get; set; }
        public List<QuestionSetupDTO> ListOfQuestions { get; set; }

    }

}
