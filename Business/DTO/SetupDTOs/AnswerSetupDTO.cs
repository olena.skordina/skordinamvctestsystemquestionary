﻿namespace Business.DTO.SetupDTOs
{
    public class AnswerSetupDTO
    {
        public int? Id { get; set; }
        public string AnswerText { get; set; }
        public bool IsCorrect { get; set; }
    }

}
