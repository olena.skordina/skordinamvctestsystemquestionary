﻿using Business.DTO;
using Business.DTO.TestRunDTOs;
using Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IUserService
    {
     //   Task<List<TestRunDTO>> ListOfUsersDTO();
        Task SaveUserToDB(UserDTO user);
        Task<UserEntity> GetUserByCredentials(string login, string password);

      //  Task<UserDTO> GetUserByLogin(string login, string password); --> to add
        
        
        
        
        //   Task UpdateTestRun(TestRunDTO testRun);
        //  Task SoftDeleteById(int testRunId);
        //   Task<TestRunDTO> StartTestRunById(int testId);
    }
}