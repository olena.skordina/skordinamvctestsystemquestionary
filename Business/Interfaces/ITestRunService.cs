﻿using Business.DTO;
using Business.DTO.TestRunDTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface ITestRunService
    {
        Task<List<TestRunDTO>> ListOfTestRunsDTO();
        Task<TestResultDTO> SaveTestRunToDB(TestRunDTO testRun);
        Task<TestRunDTO> GetTestRunById(int testRunId);
        Task UpdateTestRun(TestRunDTO testRun);
        Task SoftDeleteById(int testRunId);
        Task<TestRunDTO> StartTestRunById(int testId);
    }
}