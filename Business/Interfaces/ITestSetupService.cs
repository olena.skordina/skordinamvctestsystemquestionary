﻿using Business.DTO;
using Business.DTO.SetupDTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface ITestSetupService
    {
        Task<List<TestDTO>> ListOfTestDTO();
        Task SaveTestToDB(TestSetupDTO test);
        Task<TestSetupDTO> GetTestById(int testId);
        Task UpdateTest(TestSetupDTO test);
        Task SoftDeleteById(int testId);
    }
}