﻿using AutoMapper;
using Business.DTO;
using Business.DTO.SetupDTOs;
using Business.DTO.TestRunDTOs;
using Data.Entities;
using System;

namespace Business
{
    public class ProfileAutoMapper : Profile
    {
        public ProfileAutoMapper()
        {
            CreateMap<TestEntity, TestDTO>()
                .ReverseMap();

            CreateMap<TestEntity, TestSetupDTO>()
                .ForMember(p => p.ListOfQuestions, c => c.MapFrom(test => test.Questions))
                .ReverseMap();

            CreateMap<AnswerOptionEntity, AnswerSetupDTO>();

            CreateMap<QuestionEntity, QuestionSetupDTO>()
                .ForMember(p => p.Answers, c => c.MapFrom(question => question.Answers));


            CreateMap<AnswerSetupDTO, AnswerOptionEntity>()
                .ForMember(p => p.LastModifiedOn, c=>c.MapFrom(_ => DateTime.Now));

            CreateMap<QuestionSetupDTO, QuestionEntity>()
                .ForMember(p => p.Answers, c => c.MapFrom(question => question.Answers))
                .ForMember(p => p.LastModifiedOn, c => c.MapFrom(_ => DateTime.Now));

            CreateMap<TestRunEntity, TestRunDTO>()
                .ForMember(p => p.TestRunId, c => c.MapFrom(t => t.Id))
                .ForMember(p => p.TestName, c => c.MapFrom(t => t.TestEntity.TestName))
                .ForMember(p=> p.Questions, c=>c.MapFrom(t=> t.TestEntity.Questions))
                .ReverseMap();

            CreateMap<AnswerOptionEntity, AnswerDTO>()
                .ForMember(p => p.Id, c => c.MapFrom(t => t.Id))
                .ReverseMap();

            CreateMap<QuestionEntity, QuestionDTO>()
                .ForMember(p => p.Id, c => c.MapFrom(t => t.Id))
                .ForMember(p => p.ListOfAnswers, c => c.MapFrom(t => t.Answers))
                .ReverseMap();

            CreateMap<UserEntity, UserDTO>()
                .ReverseMap();
        }
    }
}
